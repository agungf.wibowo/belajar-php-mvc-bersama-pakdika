$( function() {
  
  $('.btn-tambah').click( e => {
    $('#formModalLongTitle').html('Tambah Data Mahasiswa');
    $('.modal-footer button[type=submit]').html('Tambah');
    $('#form-mahasiswa').attr('action',window.location.href+'/tambah');
    $('#id').val('');
    $('#nim').val('');
    $('#nama').val('');
    $('#email').val('');
    $('#jurusan').val('');
  });

  $('.btn-ubah').click( e => {
    $('#formModalLongTitle').html('Ubah Data Mahasiswa');
    $('.modal-footer button[type=submit]').html('Ubah');
    $('#form-mahasiswa').attr('action',window.location.href+'/ubah');

    const id = e.target.dataset.id;
    
    $.ajax({
      url: window.location.href+"/getubah",
      data: {id},
      method: 'post',
      dataType: 'json',
    }).done( data => {
      $('#id').val(data.id);
      $('#nim').val(data.nim);
      $('#nama').val(data.nama);
      $('#email').val(data.email);
      $('#jurusan').val(data.jurusan);
    });
  });

  $('.hapus').click( e => {
    e.preventDefault();
    swal({
      title: "Hapus Data Mahasiswa ini",
      text: "Apakah anda yakin?",
      icon: "warning",
      dangerMode: true,
      buttons: {
        cancel: {
        text: "Batal Hapus",
        value: null,
        visible: true,
        className: "",
        closeModal: true,
        },
        confirm: {
          text: "Hapus",
          value: true,
          closeModal: true
        }
      }
    }).then( val => {
      if( val ) {
        swal("Data Berhasil dihapus!", {
          icon: "success",
          buttons: false
        });
        setTimeout( () => {
          window.location.href = e.target.href
        }, 1000);
      } else {
        swal("Data Batal dihapus!", {
          className: 'batal'
        });
      }
    });
  });
});