<div class="container pt-3">
  <div class="card" style="width: 24rem;">
    <h5 class="card-header"><?=$data['mhs']['nim']?></h5>
    <div class="card-body">
      <h4 class="card-title"><?=$data['mhs']['nama']?></h4>
      <div class="row mb-3">
        <div class="col col-auto">
          <p class="card-text">Email: <?=$data['mhs']['email']?></p>
        </div>
        <div class="col col-auto">
          <p class="card-text">Jurusan : <?=$data['mhs']['jurusan']?></p>
        </div>
      </div>
      <a href="<?=BASEURL?>mahasiswa" class="btn btn-primary">Kembali</a>
    </div>
  </div>
</div>