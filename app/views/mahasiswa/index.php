<div class="container pt-3">

  <div class="row">
    <div class="col col-12">
      <?php Flasher::flash() ?>
    </div>
    <div class="col col-6">
      <!-- Button trigger modal -->
      <div class="mb-3">
        <button type="button" class="btn btn-primary btn-tambah" data-toggle="modal" data-target="#formModal">
          Tambah Mahasiswa
        </button>
      </div>
    </div>
    <div class="col col-6">
      <form action="<?=BASEURL?>mahasiswa/cari" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Cari Mahasiswa" id="keyword" name="keyword"  autocomplete="off">
          <div class="input-group-append">
            <button class="btn btn-primary" type="submit" id="tombol-cari">Cari</button>
          </div>
        </div>
      </form>
    </div>
    <div class="col-12 mb-3">
      <div class="card">
        <div class="card-header">
          <h2><?=$data['judul']?></h2>
        </div>
        <div class="card-body">
          <?php $i=1; ?>
          <ul class="list-group">
            <?php foreach($data['mhs'] as $mhs) :?>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              <?= $i++; ?>. 
              <?=$mhs['nama'];?>
              <div class="row">
                <div class="col col-auto">
                  <a href="<?=BASEURL?>mahasiswa/detail/<?=$mhs['id']?>" class="badge badge-primary text-light text-decoration-none ml-2">
                    Detail
                  </a>
                  <a href="<?=BASEURL?>mahasiswa/ubah/<?=$mhs['id']?>" class="ubah badge badge-success text-light text-decoration-none btn-ubah" data-toggle="modal" data-id="<?=$mhs['id']?>" data-target="#formModal">
                    Edit
                  </a>
                  <a href="<?=BASEURL?>mahasiswa/hapus/<?=$mhs['id']?>" class="hapus badge badge-danger text-light text-decoration-none">
                    Hapus
                  </a>
                </div>
              </div>
            </li>
            <?php endforeach;?>
          </ul>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="formModalLongTitle">Tambah Mahasiswa</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?=BASEURL?>mahasiswa/tambah" method="post" id="form-mahasiswa" >
              <div class="modal-body">
                  <input type="hidden" class="form-control" id="id" name="id">
                  <div class="form-group">
                    <label for="nim">Nim</label>
                    <input type="text" pattern="\d*" maxlength="10" class="form-control" id="nim" name="nim" aria-describedby="nimHelp"placeholder="Masukan nim" min="0" required>
                    <small id="nimHelp" class="form-text text-muted">Harus Angka</small>
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan nama" required>
                  </div>
                  <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="jhon@example.com" autocomplete="off" required>
                  </div>
                  <div class="form-group">
                    <label for="jurusan">Jurusan</label>
                    <select class="form-control" id="jurusan" name="jurusan" required>
                      <option value="" selected disabled>Pilih...</option>
                      <option value="Manajemen Informasi">Manajemen Informasi</option>
                      <option value="Sistem Informasi">Sistem Informasi</option>
                      <option value="Teknik Informatika">Teknik Informatika</option>
                    </select>
                  </div>
              </div>
              <div class="modal-footer justify-content-end">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>