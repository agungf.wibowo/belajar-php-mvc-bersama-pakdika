<?php

class About extends Controller {
  public function index( $nama = 'Agung', $status = 'Mahasiswa') {
    $data['nama'] = $nama;
    $data['status'] = $status;
    $data['judul'] = 'About';
    $this->view('templates/header');
    $this->view('about/index',$data);
    $this->view('templates/footer');
  }
  
  public function page() {
    $data['judul'] = 'About Page';
    $this->view('templates/header');
    $this->view('about/page', $data);
    $this->view('templates/footer');
  }
}